<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<html>
    <head>
        <title>Books</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="bg-dark">
        <div class="container-fluid">
            <div class="jumbotron jumbotron-fluid text-center">
                <div class="container">
                    <h1 class="display-4">Referencia de Libros</h1>
                    <p class="lead">Puedes indicar cuales son tus libros favoritos.</p>
                </div>
            </div>
            <div class="container-fluid bg-secondary ">
                <br>
                <form class="bg-white shadow-sm p-3 mb-5 bg-white rounded" action="session" method="post">
                    <div class="form-group">
                        <label>Título</label>
                        <input type="text" class="form-control" placeholder="Angeles y demonios" required="true" name="name">
                    </div>
                    <div class="form-group">
                        <label>Autor</label>
                        <input type="text" class="form-control"  placeholder="Dan Brown" name="author" required="true">
                    </div>
                    <div class="form-group">
                        <label>Fecha de publicación</label>
                        <input type="number" class="form-control" id="exampleInputPassword1" name="year" placeholder="2000" 
                               required="true">
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
                <br>
            </div>
        </div>
    </body>

</html>
