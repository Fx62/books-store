<%-- 
    Document   : show
    Created on : Feb 25, 2019, 4:17:26 PM
    Author     : fx62
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList, umg.edu.gt.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <title>Books</title>
    </head>
    <body class="bg-dark">
        <br>
        <div class="shadow-lg p-3 mb-5 bg-white rounded border border-primary">
            <%
                out.println("<table class=\"table\">");
                out.println("<thead class=\"thead-dark\">");
                out.println("<tr>");
                out.println("<th scope=\"col\">#</th>");
                out.println("<th scope=\"col\">Título</th>");
                out.println("<th scope=\"col\">Autor</th>");
                out.println("<th scope=\"col\">Año</th>");
                out.println("</tr>");
                out.println("</thead>");
                out.println("<tbody>");
                DoublyLinkedList dList = (DoublyLinkedList) session.getAttribute("books");
                ArrayList<Books> list;
                String control = (String) session.getAttribute("control");
                if(control == null){
                    control = "1";
                    session.setAttribute("control", control);
                }
                if (control.equals("1")) {
                    session.setAttribute("control", "0");
                } else {
                    session.setAttribute("control", "1");
                }
                if (control.equals("0")) {
                    list = (ArrayList) dList.showFromRight();
                } else {
                    list = (ArrayList) dList.showFromLeft();
                }
                for (int i = 0; i < list.size(); i++) {
                    Books book = list.get(i);
                    out.println("<tr>");
                    out.println("<th  scope=\"row\">" + (i + 1) + "</th>");
                    out.println("<td>" + book.getName() + "</td>");
                    out.println("<td>" + book.getAuthor() + "</td>");
                    out.println("<td>" + book.getYear() + "</td>");
                    out.println("</tr>");
                }
                out.println("</tbody>");
                out.println("</table>");
            %>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form action="show.jsp">
                    <div class="text-center">
                        <button type="submit" class="btn btn-warning mr-3" name="reverse">Revertir orden</button>
                        <a href="index.jsp" class="btn btn-primary">Inicio</a>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
