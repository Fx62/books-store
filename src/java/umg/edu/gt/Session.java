/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umg.edu.gt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fx62
 */
@WebServlet(name = "Session", urlPatterns = {"/session"})
public class Session extends HttpServlet {
    
    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String title = request.getParameter("name");
            title = new String(title.getBytes("8859_1"), "UTF8");
            String author = request.getParameter("author");
            author = new String(author.getBytes("8859_1"), "UTF8");
            int year = Integer.parseInt(request.getParameter("year"));
            HttpSession session = request.getSession();
            Books book = new Books(title, author, year);
            DoublyLinkedList list;
            list = (DoublyLinkedList)session.getAttribute("books");
            if(list == null){
                list = new DoublyLinkedList();
                session.setAttribute("books", list);
            }
            list.addLeft(book);
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Session</title>");
            out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
            out.println("<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">");
            out.println("</head>");
            out.println("<body class=\"bg-dark\">");
            //out.println("<h1>" + list.size() + "</h1>");
            out.println("<div class=\"container h-100\">");
            out.println("<br><br><br>");
            out.println("<div class=\"card text-center mx-auto\" style=\"width: 18rem;\">");
            out.println("<h6 class=\"p-3 mb-2 bg-danger text-white\">Libro agregado</h6>");
            //out.println("<div class=\"card-body\">");
            //out.println("</div>");
            out.println("<ul class=\"list-group list-group-flush\">");
            out.println("<li class=\"list-group-item\">" + title + "</li>");
            out.println("<li class=\"list-group-item\">" + author + "</li>");
            out.println("<li class=\"list-group-item\">" + year + "</li>");
            out.println("</ul>");
            out.println("<div class=\"card-body\">");
            out.println("<a href=\"index.jsp\" class=\"btn btn-primary\">Inicio</a>");
            out.println("<a href=\"show.jsp\" class=\"btn btn-info\">Mostrar</a>");
            //RequestDispatcher dis = request.getRequestDispatcher("/show.jsp");
            //dis.forward(request, response);
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
