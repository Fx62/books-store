/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umg.edu.gt;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fx62
 */
public class DoublyLinkedList {

    private Node first;
    private Node last;

    public DoublyLinkedList() {
        this.first = this.last = null;
    }

    public boolean isEmpty() {
        return first == null && last == null;
    }

    public void addLeft(Object data) {
        Node node = new Node(data);
        if (isEmpty()) {
            first = node;
        } else {
            if (last == first) {
                first.setLeft(node);
                last.setRight(first);
            }
            node.setRight(last);
            last.setLeft(node);
        }
        last = node;
        //System.out.println("First: " + first.getLeft() + "   " + first + "   " + first.getRight());
        //System.out.println("Last: " + last.getLeft() + "   " + last + "   " + last.getRight());
    }

    public void addRight(Object data) {
        Node node = new Node(data);
        if (isEmpty()) {
            last = node;
        } else {
            if (first == last) {
                last.setRight(first);
                first.setLeft(last);
            }
            node.setLeft(first);
            first.setRight(node);
        }
        first = node;
    }

    public boolean deleteLeft() {
        if (!isEmpty()) {
            last = last.getRight();
            if (last != null) {
                last.setLeft(null);
            } else {
                first = null;
            }
            return true;
        }
        return false;
    }

    public boolean deleteRight() {
        if (!isEmpty()) {
            first = first.getLeft();
            if (first != null) {
                first.setRight(null);
            } else {
                last = null;
            }
            return true;
        }
        return false;
    }

    public List<Object> showFromLeft() {
        Node temp = last;
        List<Object> students = new ArrayList<>();
        if (!isEmpty()) {
            first.setRight(null);
            while (temp != null) {
                students.add(temp.getData());
                //System.out.print(temp.getData().getId() + " - " + temp.getData().getNombre() + " -> ");
//                System.out.print("(left= " + temp.getLeft() + ")");
                temp = temp.getRight();
            }
            System.out.println();
            return students;
        }
        return null;
    }

    public List<Object> showFromRight() {
        Node temp = first;
        List<Object> students = new ArrayList<>();
        if (!isEmpty()) {
            last.setLeft(null);
            while (temp != null) {
                //System.out.print(temp.getData().getId() + " - " + temp.getData().getNombre() + " -> ");
                students.add(temp.getData());
//                System.out.print("(left= " + temp.getLeft() + ")");
                temp = temp.getLeft();
            }
            System.out.println();
            return students;
        }
        return null;
    }

    public Node getFirst() {
        return first;
    }

    public void setFirst(Node first) {
        this.first = first;
    }

    public Node getLast() {
        return last;
    }

    public void setLast(Node last) {
        this.last = last;
    }

}
